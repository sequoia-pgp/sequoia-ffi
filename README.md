# Foreign function interfaces for Sequoia PGP

This repository contains legacy foreign function interfaces (FFI) to Sequoia PGP.

The provided bindings for the C and Python programming language are discontinued in favor of point solutions and other specialized crates, as the [sequoia-openpgp](https://gitlab.com/sequoia-pgp/sequoia/-/tree/main/openpgp) crate offers a large surface to cover.

A notable example for such a solution can be found with [rpm-sequoia](https://github.com/rpm-software-management/rpm-sequoia/), which offers an implementation of RPM's PGP interface.
If you are planning on developing a point solution yourself, don't hesitate to [reach out to us](https://sequoia-pgp.org/contribute/).

For an up-to-date Python library, that offers functionality on top of `sequoia-openpgp`, please refer to [PySequoia](https://gitlab.com/sequoia-pgp/pysequoia).

